#include <map>
#include <set>
#include <list>
#include <cmath>
#include <ctime>
#include <deque>
#include <queue>
#include <stack>
#include <string>
#include <bitset>
#include <cstdio>
#include <limits>
#include <vector>
#include <climits>
#include <cstring>
#include <cstdlib>
#include <fstream>
#include <numeric>
#include <sstream>
#include <iostream>
#include <algorithm>
#include <unordered_map>
#include <stdlib.h>

using namespace std;

bool isAdmissibleOverpayment(vector<double> prices, vector<string> notes, double x) {

string j, str_2;
vector<string> temp_parsed;
vector<vector<string>> input_parsed;
double t = 0;    

    for(int i = 0;i < notes.size();i++)
        {
                j = accumulate(begin(notes[i]),end(notes[i]),j);    
                istringstream ss(j);
                while(getline(ss,str_2,' '))
                    temp_parsed.push_back(str_2);           
                input_parsed.push_back(temp_parsed);
		temp_parsed.clear();
		j.clear();      
        }
    
    for(int i = 0; i < input_parsed.size(); i++)
        for(int j = 0; j < input_parsed[i].size(); j++)
            cout << input_parsed[i][j] << " ";
        cout << "\n";
    
    vector<double> par (prices.size(), 0);
    for(int i = 0; i  < input_parsed.size(); i++)
        {
            if(input_parsed[i][1] == "higher")
            {
                string a = input_parsed[i][0];
                char* pEnd;
                double d1;
                const char* conv_my_str = a.c_str();
                d1 = strtod(conv_my_str, &pEnd); 
                double out = prices[i] / (1 + d1/100) ;
                par[i] = prices[i] - out;
                a.clear();
            }
            else if(input_parsed[i][1] == "lower")
            {
                string b = input_parsed[i][0];
                char* gEnd;
                double d2;
                const char* conv_my_str_2 = b.c_str();
                d2 = strtod(conv_my_str_2, &gEnd); 
                double out_2 = prices[i] / (1 - d2/100) ;
                par[i] = prices[i] - out_2;
                b.clear();
            }
            else if(input_parsed[i][1] == "as")
                par[i] = 0;            
        }    
    
    for(int i = 0; i < par.size(); i++)
        {
             cout << "par[i] " << par[i] << "\n";
             t += par[i];
             cout << "t" << t << "\n";
        }
    
    if(t <= x) return true;
    else return false;
}

int main()
{
vector<double> prices =  {110, 95, 70};
vector<string> notes = {"10.0% higher than in-store", 
 "5.0% lower than in-store", 
 "Same as in-store"};
double x = 5;
bool a = isAdmissibleOverpayment(prices,notes,x);
cout << a << "\n";
return 0;
} 
