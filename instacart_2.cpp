#include <map>
#include <set>
#include <list>
#include <cmath>
#include <ctime>
#include <deque>
#include <queue>
#include <stack>
#include <string>
#include <bitset>
#include <cstdio>
#include <limits>
#include <vector>
#include <climits>
#include <cstring>
#include <cstdlib>
#include <fstream>
#include <numeric>
#include <sstream>
#include <iostream>
#include <algorithm>
#include <unordered_map>
#include <stdlib.h>

using namespace std;

struct Interval {
      int start;
      int end;
      Interval() : start(0), end(0) {}
      Interval(int s, int e) : start(s), end(e) {}
};

bool busyHolidays(vector<vector<string>> shoppers, vector<vector<string>> orders, vector<int> leadTime) {

string f, str_3;
vector<string> tmp_parsed;
vector<vector<string>> inpt_parsed;
    
    for(int i = 0;i < shoppers.size();i++)
        {
                f = accumulate(begin(shoppers[i]),end(shoppers[i]),f);    
                istringstream ss(f);
                while(getline(ss,str_3,','))
                    tmp_parsed.push_back(str_3);           
                inpt_parsed.push_back(tmp_parsed);
		tmp_parsed.clear();
		f.clear();      
        }

    vector<vector<Interval>> par_;
    vector<Interval> temp_;
    for(int i = 0; i < inpt_parsed.size(); i++)
        {
        
                int m  = ((inpt_parsed[i][0][0] - '0') * 10 + (inpt_parsed[i][0][1] - '0')) * 60 + ((inpt_parsed[i][0][3] - '0') * 10 + (inpt_parsed[i][0][4] - '0')); 
                int n  = ((inpt_parsed[i][1][1] - '0') * 10 + (inpt_parsed[i][1][2] - '0')) * 60 + ((inpt_parsed[i][1][4] - '0') * 10 + (inpt_parsed[i][1][5] - '0'));
		Interval xt;                  
		xt.start = m;
                xt.end = n;
                temp_.push_back(xt);
		par_.push_back(temp_);
                temp_.clear();
            }
        

string j, str_2;
vector<string> temp_parsed;
vector<vector<string>> input_parsed;
    
    for(int i = 0;i < orders.size();i++)
        {
                j = accumulate(begin(orders[i]),end(orders[i]),j);    
                istringstream ss(j);
                while(getline(ss,str_2,','))
                    temp_parsed.push_back(str_2);           
                input_parsed.push_back(temp_parsed);
		temp_parsed.clear();
		j.clear();      
        }
    
    vector<vector<Interval>> par;
    vector<Interval> temp;
    for(int i = 0; i < input_parsed.size(); i++)
        {
        for(int j = 0; j < input_parsed[i].size(); j++)
            {
                int k  = ((input_parsed[i][0][0] - '0') * 10 + (input_parsed[i][0][1] - '0')) * 60 + ((input_parsed[i][0][3] - '0') * 10 + (input_parsed[i][0][4] - '0')); 
                int l  = ((input_parsed[i][1][1] - '0') * 10 + (input_parsed[i][1][2] - '0')) * 60 + ((input_parsed[i][1][4] - '0') * 10 + (input_parsed[i][1][5] - '0')); 
		Interval tp;                
		tp.start = k;
                tp.end = l;
		temp.push_back(tp);
                par.push_back(temp);
                temp.clear();
            }
        }

sort(par_.begin(), par_.end(), [](auto & a, auto & b){ return a[0].start < b[0].start; });
sort(par.begin(), par.end(), [](auto & a, auto & b){ return a[0].start < b[0].start; });

for(int i = 0; i < par.size(); i++)
	for(int j = 0; j < par[0].size(); j++)
		cout << par[i][0].start << par[i][0].end << "\n";

for(int i = 0; i < par_.size(); i++)
	for(int j = 0; j < par_[0].size(); j++)
		cout << par_[i][0].start << par_[i][0].end << "\n";

int count = 0;
for(int i = 0; i < par.size(); i++)
{
	for(int j = 0; j < par_.size(); j++)
	{
		if(par[i][0].start  >= par_[j][0].start && par[i][0].end  <= par_[j][0].end && par[i][0].start + leadTime[i] <= par[i][0].end) 
		{
			count++; 
			break;
		}
		if(par[i][0].start  <= par_[j][0].start && par[i][0].end  <= par_[j][0].end && par_[j][0].start + leadTime[i] <= par[i][0].end) 
		{
			count++; 
			break;
		}
		if(par[i][0].start  >= par_[j][0].start && par[i][0].end  <= par_[j][0].end && par[i][0].start + leadTime[i] <= par[i][0].end) 
		{
			count++; 
			break;
		}
		if(par[i][0].start  >= par_[j][0].start && par[i][0].start  <= par_[j][0].end && par[i][0].end  >= par_[j][0].end && par[i][0].start + leadTime[i] <= par_[j][0].end) 
		{
			count++; 
			break;
		}
	}
}
cout << "count" << count << "\n";
if(count == par.size()) return true;
return false;
}

int main()
{
vector<vector<string>> prices =  {{"13:00, 14:40"}, {"15:10, 16:00"}, {"17:50, 22:30"}};
vector<vector<string>> notes = {{"01:30, 10:00"}, {"01:30, 08:00"}};
vector<int> x = {5, 5};
bool a = busyHolidays(prices,notes,x);
cout << a << "\n";
return 0;
} 

